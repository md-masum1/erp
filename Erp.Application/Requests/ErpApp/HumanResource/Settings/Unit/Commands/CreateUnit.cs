﻿using Erp.Application.Common.Interfaces;
using Erp.Application.Common.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit.Commands
{
    
    public class CreateUnit : IRequest<Result>
    {

        public int UnitId { get; set; }
        public string UnitName { get; set; }


       public  CreateUnit(int unitId, string unitName)
       {
           UnitId = unitId;
           UnitName = unitName;
       }

    }
}
