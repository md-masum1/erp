﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Erp.Application.Common.Interfaces;
using Erp.Application.Common.Models;
using MediatR;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit.Commands
{
    public class CreateUnitHandler : IRequestHandler<CreateUnit, Result>
    {

        private readonly IUnitService _unitService;
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTime _dateTime;


        public CreateUnitHandler(IUnitService unitService, ICurrentUserService currentUserService, IDateTime dateTime)
        {
            _unitService = unitService ?? throw new ArgumentNullException(nameof(_unitService));
            _currentUserService = currentUserService ?? throw new ArgumentNullException(nameof(_currentUserService));
            _dateTime = dateTime ?? throw new ArgumentNullException(nameof(_dateTime));

        }

        public async Task<Result> Handle(CreateUnit request, CancellationToken cancellationToken)
        {

            var unit = new Domain.Entities.ErpApp.HumanResource.Settings.Unit
            {

                UnitId = request.UnitId,
                UnitName = request.UnitName,
                CreatedBy = _currentUserService.EmployeeId,
                CreateDate = _dateTime.Now,
                UpdateBy = _currentUserService.EmployeeId,
                UpdateDate = _dateTime.Now,
                HeadOfficeId = _currentUserService.HeadOfficeId,
                BranchOfficeId = _currentUserService.BranchOfficeId


            };

            var result = await _unitService.CreateUnit(unit);
            return result;
        }
    }
}
