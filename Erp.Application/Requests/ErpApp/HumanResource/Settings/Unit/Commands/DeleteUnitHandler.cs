﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Erp.Application.Common.Interfaces;
using Erp.Application.Common.Models;
using MediatR;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit.Commands
{
    public class DeleteUnitHandler : IRequestHandler<DeleteUnit, Result>
    {

        private readonly IUnitService _unitService;


        public DeleteUnitHandler(IUnitService unitService, ICurrentUserService currentUserService, IDateTime dateTime)
        {

            _unitService = unitService ?? throw new ArgumentNullException(nameof(_unitService));


        }
        public async Task<Result> Handle(DeleteUnit request, CancellationToken cancellationToken)
        {


            return await _unitService.DeleteUnit(request.Id);
        }
    }
}
