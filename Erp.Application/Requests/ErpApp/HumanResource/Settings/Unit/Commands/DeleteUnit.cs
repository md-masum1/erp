﻿using Erp.Application.Common.Interfaces;
using Erp.Application.Common.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit.Commands
{
    public class DeleteUnit : IRequest<Result>
    {
        public int Id { get; set; }

        public DeleteUnit(int id)
        {
            Id = id;

        }

    }
}
