﻿using Erp.Application.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit
{
    public interface IUnitService : IDisposable
    {
        public Task<Result> CreateUnit(Domain.Entities.ErpApp.HumanResource.Settings.Unit unit);

        public Task<Result> DeleteUnit(int id);

        public Task<Domain.Entities.ErpApp.HumanResource.Settings.Unit> GetUnitById(int id);

        public Task<IList<Domain.Entities.ErpApp.HumanResource.Settings.Unit>> GetUnit();

    }


}
