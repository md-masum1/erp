﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit.Queries
{
    public class GetUnitByIdHandler : IRequestHandler<GetUnitById, UnitDto>
    {
        private readonly IUnitService _unitService;
        private readonly IMapper _mapper;

        public GetUnitByIdHandler(IUnitService unitService, IMapper mapper)
        {

            _unitService = unitService ?? throw new ArgumentNullException(nameof(_unitService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(_mapper));
        }

        public async Task<UnitDto> Handle(GetUnitById request, CancellationToken cancellationToken)
        {
            var unit = await _unitService.GetUnitById(request.Id);
            return _mapper.Map<UnitDto>(unit);
        }
    }
}
