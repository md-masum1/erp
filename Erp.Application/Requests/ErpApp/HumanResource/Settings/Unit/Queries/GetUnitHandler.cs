﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit.Queries
{
    public class GetUnitHandler : IRequestHandler<GetUnit, IList<UnitDto>>
    {

        private readonly IUnitService _unitService;
        private readonly IMapper _mapper;

        public GetUnitHandler(IUnitService unitService, IMapper mapper)
        {

            _unitService = unitService ?? throw new ArgumentNullException(nameof(_unitService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(_mapper));

        }

        public async Task<IList<UnitDto>> Handle(GetUnit request, CancellationToken cancellationToken)
        {
            var unit = await _unitService.GetUnit();
            return _mapper.Map<IList<UnitDto>>(unit);
        }
    }
}
