﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit.Queries
{
    public class GetUnitById : IRequest<UnitDto>
    {

        public int Id { get; set; }


        public GetUnitById(int id)
        {

            Id = id;
        }

    }
}
