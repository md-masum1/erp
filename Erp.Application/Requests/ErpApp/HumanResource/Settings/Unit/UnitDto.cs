﻿using Erp.Application.Common.Mappings;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit
{
    public class UnitDto : IMapFrom<Domain.Entities.ErpApp.HumanResource.Settings.Unit>
    {


        public int UnitId { get; set; }
        public string UnitName { get; set; }






    }
}
