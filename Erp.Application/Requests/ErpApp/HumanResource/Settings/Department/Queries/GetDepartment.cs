﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit;
using Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit.Queries;
using MediatR;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Department.Queries
{
    public class GetDepartment : IRequest<IList<DepartmentDto>>
    {
        public GetDepartment()
        {


        }
    }
}
