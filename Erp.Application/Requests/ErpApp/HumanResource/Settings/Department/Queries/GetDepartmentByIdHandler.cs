﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Department.Queries
{
    public class GetDepartmentByIdHandler : IRequestHandler<GetDepartmentById, DepartmentDto>
    {
        private readonly IDepartmentService _departmentService;
        private readonly IMapper _mapper;

        public GetDepartmentByIdHandler(IDepartmentService departmentService, IMapper mapper)
        {

            _departmentService = departmentService ?? throw new ArgumentNullException(nameof(_departmentService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(_mapper));
        }

        public async Task<DepartmentDto> Handle(GetDepartmentById request, CancellationToken cancellationToken)
        {
            var department = await _departmentService.GetDepartmentById(request.Id);
            return _mapper.Map<DepartmentDto>(department);
        }
    }
}
