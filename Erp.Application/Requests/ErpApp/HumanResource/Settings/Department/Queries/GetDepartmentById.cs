﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit;
using MediatR;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Department.Queries
{
    public class GetDepartmentById : IRequest<DepartmentDto>
    {

        public int Id { get; set; }


        public GetDepartmentById(int id)
        {
            Id = id;

        }

    }
}
