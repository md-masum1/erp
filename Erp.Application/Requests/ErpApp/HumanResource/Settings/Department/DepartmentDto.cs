﻿using System;
using System.Collections.Generic;
using System.Text;
using Erp.Application.Common.Mappings;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Department
{
    public class DepartmentDto : IMapFrom<Domain.Entities.ErpApp.HumanResource.Settings.Department>
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
    }

}
