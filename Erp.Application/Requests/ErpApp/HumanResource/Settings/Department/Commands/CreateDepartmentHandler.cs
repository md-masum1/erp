﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Erp.Application.Common.Interfaces;
using Erp.Application.Common.Models;
using MediatR;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Department.Commands
{
    public class CreateUnitHandler : IRequestHandler<CreateDepartment, Result>
    {

        private readonly IDepartmentService _departmentService;
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTime _dateTime;


        public CreateUnitHandler(IDepartmentService departmentService, ICurrentUserService currentUserService, IDateTime dateTime)
        {
            _departmentService = departmentService ?? throw new ArgumentNullException(nameof(_departmentService));
            _currentUserService = currentUserService ?? throw new ArgumentNullException(nameof(_currentUserService));
            _dateTime = dateTime ?? throw new ArgumentNullException(nameof(_dateTime));

        }

        public async Task<Result> Handle(CreateDepartment request, CancellationToken cancellationToken)
        {

            var department = new Domain.Entities.ErpApp.HumanResource.Settings.Department
            {

                DepartmentId = request.DepartmentId,
                DepartmentName = request.DepartmentName,
                CreatedBy = _currentUserService.EmployeeId,
                CreateDate = _dateTime.Now,
                UpdateBy = _currentUserService.EmployeeId,
                UpdateDate = _dateTime.Now,
                HeadOfficeId = _currentUserService.HeadOfficeId,
                BranchOfficeId = _currentUserService.BranchOfficeId


            };

            var result = await _departmentService.CreateDepartment(department);
            return result;
        }
    }
}
