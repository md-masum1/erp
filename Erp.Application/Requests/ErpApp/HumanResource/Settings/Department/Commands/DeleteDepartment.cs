﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Erp.Application.Common.Interfaces;
using Erp.Application.Common.Models;
using Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit;
using Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit.Commands;
using MediatR;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Department.Commands
{
    public class DeleteDepartment : IRequest<Result>
    {

        public int Id { get; set; }

        public DeleteDepartment(int id)
        {

            Id = id;
        }

    }
}
