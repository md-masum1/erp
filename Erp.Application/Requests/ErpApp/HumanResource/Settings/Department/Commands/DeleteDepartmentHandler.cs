﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Erp.Application.Common.Interfaces;
using Erp.Application.Common.Models;
using MediatR;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Department.Commands
{
    public class DeleteDepartmentHandler : IRequestHandler<DeleteDepartment, Result>
    {

        private readonly IDepartmentService _departmentService;


        public DeleteDepartmentHandler(IDepartmentService departmentService, ICurrentUserService currentUserService, IDateTime dateTime)
        {

            _departmentService = departmentService ?? throw new ArgumentNullException(nameof(_departmentService)); ;


        }
        public async Task<Result> Handle(DeleteDepartment request, CancellationToken cancellationToken)
        {


            return await _departmentService.DeleteDepartment(request.Id);
        }
    }
}
