﻿using Erp.Application.Common.Interfaces;
using Erp.Application.Common.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Department.Commands
{
    public class CreateDepartment : IRequest<Result>
    {

        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }

        public CreateDepartment(int departmentId, string departmentName)
        {
            DepartmentId = departmentId;
            DepartmentName = departmentName;

        }

    }

}

