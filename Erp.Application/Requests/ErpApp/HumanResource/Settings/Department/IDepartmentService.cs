﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Erp.Application.Common.Models;

namespace Erp.Application.Requests.ErpApp.HumanResource.Settings.Department
{
    public interface IDepartmentService : IDisposable
    {
      
        public Task<Result> CreateDepartment(Domain.Entities.ErpApp.HumanResource.Settings.Department department);
        public Task<Result> DeleteDepartment(int Id);

        public Task<Domain.Entities.ErpApp.HumanResource.Settings.Department> GetDepartmentById(int Id);
        public Task<IList<Domain.Entities.ErpApp.HumanResource.Settings.Department>> GetDepartment();


    }
}
