﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;

namespace Erp.Application.Requests.Demo.Queries
{
    public class GetDemoCustomerQuery : IRequest<IEnumerable<DemoCustomerToReturnDto>>
    {
        public class GetDemoCustomerQueryHandler : IRequestHandler<GetDemoCustomerQuery, IEnumerable<DemoCustomerToReturnDto>>
        {
            private readonly IDemoCustomer _demoCustomer;
            private readonly IMapper _mapper;

            public GetDemoCustomerQueryHandler(IDemoCustomer demoCustomer, IMapper mapper)
            {
                _demoCustomer = demoCustomer;
                _mapper = mapper;
            }
            public async Task<IEnumerable<DemoCustomerToReturnDto>> Handle(GetDemoCustomerQuery request, CancellationToken cancellationToken)
            {
                var data = await _demoCustomer.GetDemoCustomer();
                return _mapper.Map<IEnumerable<DemoCustomerToReturnDto>>(data);
            }
        }
    }
}
