﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Erp.Application.Common.Models;
using Erp.Application.Requests.Demo.Commands;
using Erp.Domain.Entities.Demo;

namespace Erp.Application.Requests.Demo
{
    public interface IDemoCustomer: IDisposable
    {
        Task<List<DemoCustomer>> GetDemoCustomer();
        Task<Result> DemoCustomerToCreate(CreateDemoCustomerCommand demoCustomer);
    }
}
