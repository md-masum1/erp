﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Erp.Application.Common.Models;
using Erp.Domain.Common;
using Erp.Domain.Entities.Demo;
using MediatR;

namespace Erp.Application.Requests.Demo.Commands
{
    public class CreateDemoCustomerCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public DemoCustomer1 DemoCustomer1 { get; set; }

        public class CreateDemoCustomerCommandHandler : IRequestHandler<CreateDemoCustomerCommand, Result>
        {
            private readonly IDemoCustomer _demoCustomer;

            public CreateDemoCustomerCommandHandler(IDemoCustomer demoCustomer)
            {
                _demoCustomer = demoCustomer;
            }
            public async Task<Result> Handle(CreateDemoCustomerCommand request, CancellationToken cancellationToken)
            {
                return await _demoCustomer.DemoCustomerToCreate(request);
            }
        }
    }
}
