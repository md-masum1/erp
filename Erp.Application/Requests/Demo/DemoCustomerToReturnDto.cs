﻿using Erp.Application.Common.Mappings;
using Erp.Domain.Entities.Demo;

namespace Erp.Application.Requests.Demo
{
    public class DemoCustomerToReturnDto : IMapFrom<DemoCustomer>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
