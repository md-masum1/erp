﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Application.Common.Messages
{
    public enum SystemMessageType
    {
        Error,
        Success,
        Info,
        Warning
    }
}
