﻿using System.Threading.Tasks;
using Erp.Application.Common.Models;
using Erp.Application.Auth;

namespace Erp.Application.Common.Interfaces
{
    public interface IIdentityService
    {
        Task<object> Login(UserForLoginDto userForLogin);
        Task<(Result Result, int UserId)> Register(UserForRegisterDto userForRegister);
    }
}
