﻿using System.Threading;
using System.Threading.Tasks;
using Erp.Application.Common.Models;

namespace Erp.Application.Common.Interfaces
{
    public interface ILogToDatabaseService
    {
        Task<Result> Save(RequestLoggerEntity loggerEntity, CancellationToken cancellationToken);
    }
}
