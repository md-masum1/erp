﻿using System.Threading;
using System.Threading.Tasks;
using Erp.Application.Common.Interfaces;
using Erp.Application.Common.Models;
using MediatR.Pipeline;
using Microsoft.Extensions.Logging;

namespace Erp.Application.Common.Behaviours
{
    public class RequestLogger<TRequest> : IRequestPreProcessor<TRequest>
    {
        private readonly ILogger _logger;
        private readonly ICurrentUserService _currentUserService;
        private readonly ILogToDatabaseService _logToDatabaseService;

        public RequestLogger(ILogger<TRequest> logger, ICurrentUserService currentUserService, ILogToDatabaseService logToDatabaseService)
        {
            _logger = logger;
            _currentUserService = currentUserService;
            _logToDatabaseService = logToDatabaseService;
        }

        public async Task Process(TRequest request, CancellationToken cancellationToken)
        {
            var requestName = typeof(TRequest).Name;
            var userId = _currentUserService.UserId;
            var userName = _currentUserService.UserName;

            var logToDatabase = new RequestLoggerEntity
            {
                RequestName = requestName,
                UserId = userId,
                UserName = userName
            };

            await _logToDatabaseService.Save(logToDatabase, cancellationToken);

            _logger.LogInformation("SaRa Ecommerce Request: {Name} {@UserId} {@UserName} {@Request}",
                requestName, userId, userName, request);
        }
    }
}
