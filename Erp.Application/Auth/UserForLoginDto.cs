﻿using Erp.Application.Common.Mappings;
using Erp.Application.Auth.Commands;

namespace Erp.Application.Auth
{
    public class UserForLoginDto : IMapFrom<LoginCommand>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
