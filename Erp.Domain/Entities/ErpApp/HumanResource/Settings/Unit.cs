﻿using Erp.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Domain.Entities.ErpApp.HumanResource.Settings
{
    [Table("L_UNIT")]
    public class Unit : AuditableEntity
    {

        
        [Column("UNIT_ID")]
        public int UnitId { get; set; }

        [Column("UNIT_NAME")]
        public string UnitName { get; set; }
 
       
        public virtual ICollection<Department> Departments { get; set; }
        

    }
}
