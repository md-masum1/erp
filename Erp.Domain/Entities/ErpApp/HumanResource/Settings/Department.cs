﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Erp.Domain.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Erp.Domain.Entities.ErpApp.HumanResource.Settings
{
   [Table("L_DEPARTMENT")]
    public class Department : AuditableEntity
    {

        [Column("DEPARTMENT_ID")] 
        public int DepartmentId { get; set; }


        [Column("DEPARTMENT_NAME")]
        public string DepartmentName { get; set; }


        [Column("UNIT_ID")]
        public int UnitId { get; set; }

        public virtual Unit Units { get; set; }





    }

    //This bellow part is used for Relation between tables
    public class DepartmentMap : IEntityTypeConfiguration<Department>
    {
        public void Configure(EntityTypeBuilder<Department> builder)
        {

            builder.HasOne(x => x.Units).WithMany(x => x.Departments).HasForeignKey(x => x.UnitId);
        }
    }
}
