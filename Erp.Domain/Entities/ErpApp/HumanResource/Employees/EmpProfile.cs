﻿using Erp.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Domain.Entities.ErpApp.HumanResource.Employees
{
    [Table("EMPLOYEE_PROFILE")]
    public class EmpProfile : AuditableEntity
    {

        [Column("EMPLOYEE_ID")]
        public string EmployeeId { get; set; }

        [Column("EMPLOYEE_NAME")]
        public string EmployeeName { get; set; }

        [Column("EMPLOYEE_NAME_BANGLA")]
        public string EmployeeNameBangla { get; set; }


        [Column("FATHER_NAME")]
        public string FatherName { get; set; }

        [Column("MOTHER_NAME")]
        public string MotherName { get; set; }

        [Column("DATE_OF_BIRTH")]
        public DateTime DateOfBirth { get; set; }


        [Column("CARD_NO")]
        public string CardNo { get; set; }

        [Column("PUNCH_CODE")]
        public string PunchCode { get; set; }

        [Column("GENDER_ID")]
        public int GenderId { get; set; }


        [Column("BLOOD_GROUP_ID")]
        public int BloodGroupId { get; set; }

        [Column("MARITAL_STATUS_ID")]
        public int MaritalStatusId { get; set; }

        [Column("SPOUSE_NAME")]
        public string SpouseName { get; set; }

        public ICollection<EmpAddress> empAddresses { get; set; }
        public ICollection<EmpContact> empContacts { get; set; }






    }
}
