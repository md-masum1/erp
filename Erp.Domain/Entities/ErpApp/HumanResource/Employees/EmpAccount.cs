﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Domain.Entities.ErpApp.HumanResource.Employees
{
    public class EmpAccount
    {
        [Table("EMPLOYEE_ACCOUNT")]
        public class EmpAddress
        {
            [Column("EMPLOYEE_ACCOUNT_ID")] 
            public int EmployeeAccountId { get; set; }


            [Column("EMPLOYEE_ID")] 
            public string EmployeeId { get; set; }

            [Column("BANK_ACCOUNT_NO")]
            public string BankAccountNo { get; set; }

            [Column("BKASH_ACCOUNT_NO")]
            public string BkashAccountNo { get; set; }

           



        }
    }

}
