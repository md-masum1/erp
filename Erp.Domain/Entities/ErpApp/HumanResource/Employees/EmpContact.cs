﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Domain.Entities.ErpApp.HumanResource.Employees
{

    [Table("EMPLOYEE_CONTACT")]

    public class EmpContact
    {
        [Column("EMPLOYEE_CONTACT_ID")]
        public int EmpContactId { get; set; }

        [Column("EMPLOYEE_ID")]
        public string EmployeeId { get; set; }

        [Column("PARENTS_CONTACT_NO")]
        public string ParentContactNo { get; set; }

        [Column("MAIL_ADDRESS")]
        public string MailAddress { get; set; }

        [Column("CONTACT_NO")]
        public string ContactNo { get; set; }

        [Column("EMERGENCY_CONTACT_NO")]
        public string EmergencyContactNo { get; set; }
        [Column("CORPORATE_CONTACT_NO")]
        public string CorporateContactNo { get; set; }

        [Column("PASSPORT_NO")]
        public string PassportNo { get; set; }

        [Column("DRIVING_LICENSE_NO")]
        public string DrivingLicenseNo { get; set; }

        [Column("NID_NO")]
        public string NidNo { get; set; }

        [Column("TIN_NO")]
        public string TiNo { get; set; }



    }
}
