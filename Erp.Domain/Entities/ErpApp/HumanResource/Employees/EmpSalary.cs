﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Domain.Entities.ErpApp.HumanResource.Employees
{
    [Table("EMPLOYEE_SALARY")]
    public class EmpSalary
    {
        [Column("EMPLOYEE_SALARY_ID")]
        public int EmployeeSalaryId { get; set; }

        [Column("EMPLOYEE_ID")]
        public string EmployeeId { get; set; }

        [Column("JOINING_SALARY")]
        public string JoiningSalary { get; set; }

        [Column("FIRST_SALARY")]
        public string FirstSalary { get; set; }

        [Column("GROSS_SALARY")]
        public string GrossSalary { get; set; }




    }
}
