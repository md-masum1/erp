﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Domain.Entities.ErpApp.HumanResource.Employees
{
    [Table("EMPLOYEE_ADDRESS")]
    public class EmpAddress
    {
        [Column("EMPLOYEE_ADDRESS_ID")]
        public int EmpAddressId { get; set; }


        [Column("EMPLOYEE_ID")]
        public string EmployeeId { get; set; }

        [Column("DISTRICT_ID")]
        public int DistrictId { get; set; }

        [Column("DIVISION_ID")]
        public int DivisionId { get; set; }

        [Column("RELIGION_ID")]
        public int ReligionId { get; set; }

        [Column("COUNTRY_ID")]
        public int CountryId { get; set; }

        [Column("PRESENT_ADDRESS")]
        public string PresentAddress { get; set; }

        [Column("PERMANENT_ADDRESS")]
        public string PermanentAddress { get; set; }




    }
}
