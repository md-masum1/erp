﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Erp.Domain.Entities.Demo
{
    [Table("DEMO_CUSTOMER1")]
    public class DemoCustomer1
    {
        [Column("ID")]
        public int Id { get; set; }
        [Column("ADDRESS")]
        public string Address { get; set; }
        [Column("CITY")]
        public string City { get; set; }
        [Column("COUNTRY")]
        public string Country { get; set; }
    }
}
