﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Erp.Domain.Entities.Demo
{
    [Table("DEMO_CUSTOMER")]
    public class DemoCustomer
    {
        [Column("ID")]
        public int Id { get; set; }
        [Column("NAME")]
        public string Name { get; set; }
        [Column("EMAIL")]
        public string Email { get; set; }
        public virtual DemoCustomer1 DemoCustomer1 { get; set; }
        [Column("DEMO_CUSTOMER1_ID")]
        public int DemoCustomer1Id { get; set; }
    }
}
