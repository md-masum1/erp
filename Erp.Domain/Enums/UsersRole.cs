﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Domain.Enums
{
    public enum UsersRole
    {
        Admin,
        Employee
    }
}
