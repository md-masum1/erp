﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Erp.Application.Common.Models;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;

namespace Erp.Infrastructure.Persistence
{
    public abstract class OracleDbContext<TEntity> where TEntity : class
    {
        private readonly OracleConnection _con;
        private OracleTransaction _trans;

        protected OracleDbContext(IConfiguration configuration)
        {
            var connectionString = configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            _con = new OracleConnection(connectionString);
        }

        private async Task ConnectionOpenAsync()
        {
            if (_con.State == ConnectionState.Closed)
                await _con.OpenAsync();
        }

        private void ConnectionClose()
        {
            if (_con.State == ConnectionState.Open)
                _con.Close();
        }

        public async Task<IEnumerable<TEntity>> GetListAsync(string sqlQuery, DynamicParameters parameter)
        {
            await using var connection = _con;

            try
            {
                await ConnectionOpenAsync();
                DefaultTypeMap.MatchNamesWithUnderscores = true;
                var resultList = await connection.QueryAsync<TEntity>(sqlQuery, parameter);
                ConnectionClose();
                return resultList;
            }
            catch (Exception ex)
            {
                ConnectionClose();
                throw new Exception(ex.Message);
            }
            finally
            {
                ConnectionClose();
            }
        }

        public async Task<IEnumerable<T>> GetListAsync<T>(string sqlQuery, DynamicParameters parameter)
        {
            await using var connection = _con;

            try
            {
                await ConnectionOpenAsync();
                DefaultTypeMap.MatchNamesWithUnderscores = true;
                var resultList = await connection.QueryAsync<T>(sqlQuery, parameter);
                ConnectionClose();
                return resultList;
            }
            catch (Exception ex)
            {
                ConnectionClose();
                throw new Exception(ex.Message);
            }
            finally
            {
                ConnectionClose();
            }
        }

        public async Task<TEntity> GetSingleAsync(string sqlQuery, DynamicParameters parameter)
        {
            await using var connection = _con;

            try
            {
                await ConnectionOpenAsync();
                DefaultTypeMap.MatchNamesWithUnderscores = true;
                var resultList = await connection.QueryFirstOrDefaultAsync<TEntity>(sqlQuery, parameter);
                ConnectionClose();
                return resultList;
            }
            catch (Exception ex)
            {
                ConnectionClose();
                throw new Exception(ex.Message);
            }
            finally
            {
                ConnectionClose();
            }
        }

        public async Task<T> GetSingleAsync<T>(string sqlQuery, DynamicParameters parameter)
        {
            await using var connection = _con;

            try
            {
                await ConnectionOpenAsync();
                DefaultTypeMap.MatchNamesWithUnderscores = true;
                var resultList = await connection.QueryFirstOrDefaultAsync<T>(sqlQuery, parameter);
                ConnectionClose();
                return resultList;
            }
            catch (Exception ex)
            {
                ConnectionClose();
                throw new Exception(ex.Message);
            }
            finally
            {
                ConnectionClose();
            }
        }

        public async Task<(Task<IEnumerable<T1>>, Task<IEnumerable<T2>>)> GetMultipleAsync<T1, T2>(string sqlQuery, DynamicParameters parameter)
        {
            await using var connection = _con;

            try
            {
                await ConnectionOpenAsync();
                DefaultTypeMap.MatchNamesWithUnderscores = true;
                var resultList = await connection.QueryMultipleAsync(sqlQuery, parameter);

                var t1 = resultList.ReadAsync<T1>();
                var t2 = resultList.ReadAsync<T2>();

                ConnectionClose();
                return (t1, t2);
            }
            catch (Exception ex)
            {
                ConnectionClose();
                throw new Exception(ex.Message);
            }
            finally
            {
                ConnectionClose();
            }
        }

        public async Task<(Task<IEnumerable<T1>>, Task<IEnumerable<T2>>, Task<IEnumerable<T2>>)> GetMultipleAsync<T1, T2, T3>(string sqlQuery, DynamicParameters parameter)
        {
            await using var connection = _con;

            try
            {
                await ConnectionOpenAsync();
                DefaultTypeMap.MatchNamesWithUnderscores = true;
                var resultList = await connection.QueryMultipleAsync(sqlQuery, parameter);

                var t1 = resultList.ReadAsync<T1>();
                var t2 = resultList.ReadAsync<T2>();
                var t3 = resultList.ReadAsync<T2>();

                ConnectionClose();
                return (t1, t2, t3);
            }
            catch (Exception ex)
            {
                ConnectionClose();
                throw new Exception(ex.Message);
            }
            finally
            {
                ConnectionClose();
            }
        }

        public async Task<string> GetSingleStringAsync(string sqlQuery, DynamicParameters parameter)
        {
            await using var connection = _con;

            try
            {
                await ConnectionOpenAsync();
                var resultList = await connection.QueryFirstOrDefaultAsync<string>(sqlQuery, parameter);
                ConnectionClose();
                return resultList;
            }
            catch (Exception ex)
            {
                ConnectionClose();
                throw new Exception(ex.Message);
            }
            finally
            {
                ConnectionClose();
            }
        }

        public async Task<Int32> GetSingleIntAsync(string sqlQuery, DynamicParameters parameter)
        {
            await using var connection = _con;

            try
            {
                await ConnectionOpenAsync();
                var resultList = await connection.QueryFirstOrDefaultAsync<Int32>(sqlQuery, parameter);
                ConnectionClose();
                return resultList;
            }
            catch (Exception ex)
            {
                ConnectionClose();
                throw new Exception(ex.Message);
            }
            finally
            {
                ConnectionClose();
            }
        }

        public async Task<Result> SetSingleAsync(string sqlQuery, DynamicParameters parameter)
        {
            await using var connection = _con;

            try
            {
                await ConnectionOpenAsync();

                await using (_trans = connection.BeginTransaction())
                {
                    try
                    {
                        var affectedRows = await connection.ExecuteAsync(sqlQuery, parameter, commandType: CommandType.StoredProcedure);
                        await _trans.CommitAsync();
                        ConnectionClose();
                        var result = parameter.Get<string>("P_MESSAGE");
                        return Result.Success(result);
                    }
                    catch (Exception ex)
                    {
                        await _trans.RollbackAsync();
                        return Result.Failure(new List<string> { ex.Message });
                    }
                }
            }
            catch (Exception ex)
            {
                ConnectionClose();
                return Result.Failure(new List<string> { ex.Message });
            }
            finally
            {
                ConnectionClose();
            }
        }

        public async Task<Result> SetMultipleAsync(string sqlQuery, List<DynamicParameters> parameter)
        {
            await using var connection = _con;

            try
            {
                await ConnectionOpenAsync();

                await using (_trans = connection.BeginTransaction())
                {
                    try
                    {
                        var affectedRows = await connection.ExecuteAsync(sqlQuery, parameter, commandType: CommandType.StoredProcedure);
                        await _trans.CommitAsync();
                        ConnectionClose();
                        return Result.Success();
                    }
                    catch (Exception ex)
                    {
                        await _trans.RollbackAsync();
                        return Result.Failure(new List<string>{ex.Message });
                    }
                }
            }
            catch (Exception ex)
            {
                ConnectionClose();
                return Result.Failure(new List<string> { ex.Message });
            }
            finally
            {
                ConnectionClose();
            }
        }
    }
}
