﻿using Microsoft.EntityFrameworkCore.Migrations;
using Oracle.EntityFrameworkCore.Metadata;

namespace Erp.Infrastructure.Migrations
{
    public partial class AddDamoCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DEMO_CUSTOMER1",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Oracle:ValueGenerationStrategy", OracleValueGenerationStrategy.IdentityColumn),
                    ADDRESS = table.Column<string>(nullable: true),
                    CITY = table.Column<string>(nullable: true),
                    COUNTRY = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DEMO_CUSTOMER1", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "DEMO_CUSTOMER",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Oracle:ValueGenerationStrategy", OracleValueGenerationStrategy.IdentityColumn),
                    NAME = table.Column<string>(nullable: true),
                    EMAIL = table.Column<string>(nullable: true),
                    DEMO_CUSTOMER1_ID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DEMO_CUSTOMER", x => x.ID);
                    table.ForeignKey(
                        name: "FK_DEMO_CUSTOMER_DEMO_CUSTOMER1_DEMO_CUSTOMER1_ID",
                        column: x => x.DEMO_CUSTOMER1_ID,
                        principalTable: "DEMO_CUSTOMER1",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DEMO_CUSTOMER_DEMO_CUSTOMER1_ID",
                table: "DEMO_CUSTOMER",
                column: "DEMO_CUSTOMER1_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DEMO_CUSTOMER");

            migrationBuilder.DropTable(
                name: "DEMO_CUSTOMER1");
        }
    }
}
