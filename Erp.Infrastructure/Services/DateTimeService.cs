﻿using System;
using Erp.Application.Common.Interfaces;

namespace Erp.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
