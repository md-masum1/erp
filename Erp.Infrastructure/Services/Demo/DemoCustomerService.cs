﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Erp.Application.Common.Models;
using Erp.Application.Requests.Demo;
using Erp.Application.Requests.Demo.Commands;
using Erp.Domain.Entities.Demo;
using Erp.Domain.Entities.ErpApp.HumanResource.Settings;
using Erp.Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Erp.Infrastructure.Services.Demo
{
    public class DemoCustomerService : OracleDbContext<DemoCustomer>, IDemoCustomer
    {
        private readonly ApplicationDbContext _context;

        public DemoCustomerService(IConfiguration configuration, ApplicationDbContext context) : base(configuration)
        {
            _context = context;
        }

        public void Dispose()
        {
            _context.Dispose();

        }

        public async Task<Result> CreateUnit(Unit unit)
        {

            string query = "pro_unit_save";

            DynamicParameters parameter = new DynamicParameters();

            parameter.Add("p_unit_id", unit.UnitId, DbType.String, ParameterDirection.Input);
            parameter.Add("p_unit_name", unit.UnitName, DbType.String, ParameterDirection.Input);


            parameter.Add("p_update_by", unit.UpdateBy, DbType.String, ParameterDirection.Input);
            parameter.Add("p_head_office_id", unit.HeadOfficeId, DbType.String, ParameterDirection.Input);
            parameter.Add("p_branch_office_id", unit.BranchOfficeId, DbType.String, ParameterDirection.Input);



            parameter.Add("P_MESSAGE", "", DbType.String, ParameterDirection.Output);

            return await SetSingleAsync(query, parameter);


        }

        public async Task<List<DemoCustomer>> GetDemoCustomer()
        {
            return await _context.DemoCustomers.ToListAsync();
        }

        public async Task<Result> DemoCustomerToCreate(CreateDemoCustomerCommand demoCustomer)
        {
            string query = "PRO_DEMO_CUSTOMER_SAVE";

            DynamicParameters parameter = new DynamicParameters();

            parameter.Add("P_ID", demoCustomer.Id, DbType.String, ParameterDirection.Input);
            parameter.Add("P_ADDRESS", demoCustomer.DemoCustomer1.Address, DbType.String, ParameterDirection.Input);


            parameter.Add("P_CITY", demoCustomer.DemoCustomer1.City, DbType.String, ParameterDirection.Input);
            parameter.Add("P_COUNTRY", demoCustomer.DemoCustomer1.Country, DbType.String, ParameterDirection.Input);
            parameter.Add("P_NAME", demoCustomer.Name, DbType.String, ParameterDirection.Input);
            parameter.Add("P_EMAIL", demoCustomer.Email, DbType.String, ParameterDirection.Input);

            parameter.Add("P_MESSAGE", "", DbType.String, ParameterDirection.Output);

            return await SetSingleAsync(query, parameter);
        }
    }
}
