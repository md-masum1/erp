﻿ using Erp.Application.Common.Models;
using Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit;
using Erp.Domain.Entities.ErpApp.HumanResource.Settings;
using Erp.Infrastructure.Persistence;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Data;
using System.Linq;

namespace Erp.Infrastructure.Services.ErpApp.HumanResource
{
    public class UnitService : OracleDbContext<Unit>, IUnitService
    {

        private readonly ApplicationDbContext _context;
        public UnitService(IConfiguration configuration, ApplicationDbContext context) : base(configuration)
        {

            _context = context;

        }

        public void Dispose()
        {
            _context.Dispose();

        }


        public async Task<Result> CreateUnit(Unit unit)
        {

            string query = "pro_unit_save";

            DynamicParameters parameter = new DynamicParameters();

            parameter.Add("p_unit_id", unit.UnitId, DbType.String, ParameterDirection.Input);
            parameter.Add("p_unit_name", unit.UnitName, DbType.String, ParameterDirection.Input);


            parameter.Add("p_update_by", unit.UpdateBy, DbType.String, ParameterDirection.Input);
            parameter.Add("p_head_office_id", unit.HeadOfficeId, DbType.String, ParameterDirection.Input);
            parameter.Add("p_branch_office_id", unit.BranchOfficeId, DbType.String, ParameterDirection.Input);



            parameter.Add("P_MESSAGE", "", DbType.String, ParameterDirection.Output);

            return await SetSingleAsync(query, parameter);


        }

        public async Task<Result> DeleteUnit(int id)
        {
            string query = "pro_unit_delete";

            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("p_unit_Id", id, DbType.String, ParameterDirection.Input);

           
            parameter.Add("P_MESSAGE", "", DbType.String, ParameterDirection.Output);


            return await SetSingleAsync(query, parameter);

            
        }

        
        public async Task<IList<Unit>> GetUnit()
        {
            string query = " select * from L_UNIT ORDER BY UNIT_NAME";

            var units = await GetListAsync(query, null);

            return units.ToList();
        }

        public async Task<Unit> GetUnitById(int id)
        {
            string query = " select * from L_UNIT where unit_id = :unit_id ORDER BY UNIT_NAME ";

            DynamicParameters parameters = new DynamicParameters();

            parameters.Add(":unit_id", id);

            return await GetSingleAsync(query, parameters);
        }
    }
}
