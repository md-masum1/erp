﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Erp.Application.Common.Models;
using Erp.Application.Requests.ErpApp.HumanResource.Settings.Department;
using Erp.Domain.Entities.ErpApp.HumanResource.Settings;
using Erp.Infrastructure.Persistence;
using Microsoft.Extensions.Configuration;

namespace Erp.Infrastructure.Services.ErpApp.HumanResource
{
    public class DepartmentService : OracleDbContext<Department>, IDepartmentService
    {

        private readonly ApplicationDbContext _context;
        public DepartmentService(IConfiguration configuration, ApplicationDbContext context) : base(configuration)
        {

            _context = context;

        }

        public void Dispose()
        {
            _context.Dispose();

        }

        public async Task<Result> CreateDepartment(Department department)
        {
            string query = "pro_department_save";

            DynamicParameters parameter = new DynamicParameters();

            parameter.Add("p_department_id", department.DepartmentId, DbType.String, ParameterDirection.Input);
            parameter.Add("p_department_name", department.DepartmentName, DbType.String, ParameterDirection.Input);


            parameter.Add("p_update_by", department.UpdateBy, DbType.String, ParameterDirection.Input);
            parameter.Add("p_head_office_id", department.HeadOfficeId, DbType.String, ParameterDirection.Input);
            parameter.Add("p_branch_office_id", department.BranchOfficeId, DbType.String, ParameterDirection.Input);



            parameter.Add("P_MESSAGE", "", DbType.String, ParameterDirection.Output);

            return await SetSingleAsync(query, parameter);
        }

        public  async Task<Result> DeleteDepartment(int id)
        {
            string query = "pro_department_delete";

            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("p_department_id", id, DbType.String, ParameterDirection.Input);


            parameter.Add("P_MESSAGE", "", DbType.String, ParameterDirection.Output);


            return await SetSingleAsync(query, parameter);
        }

       
        public async Task<IList<Department>> GetDepartment()
        {
            string query = " select * from L_DEPARTMENT ORDER BY DEPARTMENT_NAME";

            var departments = await GetListAsync(query, null);

            return departments.ToList();
        }

        public async Task<Department> GetDepartmentById(int id)
        {
            string query = " select * from L_DEPARTMENT where department_id = :department_id ORDER BY DEPARTMENT_NAME ";

            DynamicParameters parameters = new DynamicParameters();

            parameters.Add(":department_id", id);

            return await GetSingleAsync(query, parameters);
        }
    }
}
