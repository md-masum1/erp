# ERP

This project was create with dotnet core 3.1 and Oracle 19C Database.

## Create Database

Open Sql Plus as Administrator and run following command...
```
create tablespace ERP_DB
datafile 'ERP_DB.DBF' 
size 10M
autoextend on;

alter database default tablespace ERP_DB;

alter session set "_ORACLE_SCRIPT"=true;

create user ERP
identified by ERP
default tablespace ERP_DB
temporary tablespace temp;

GRANT dba to ERP;
```

## Create Default Table

Go to `Package Manager Console` set `Erp.Infrastructure` as Default project and Run `add-migration InitialCreate` 


## Create Test Product Procedure

Open toad and run this script 

```
CREATE OR REPLACE PROCEDURE ERP.pro_product_save (
    p_ProductId     IN     VARCHAR2,
    p_CreatedBy     IN     VARCHAR2,
    p_CreateDate    IN     TIMESTAMP,
    p_UpdateBy      IN     VARCHAR2,
    p_UpdateDate    IN     TIMESTAMP,
    p_ProductName   IN     VARCHAR2,
    p_Price         IN     VARCHAR2,
    p_Description   IN     VARCHAR2,
    p_message          OUT VARCHAR2)
AS
BEGIN
    IF p_ProductId > 0
    THEN
        UPDATE PRODUCT
           SET UPDATE_BY = p_UpdateBy,
               UPDATE_DATE = p_UpdateDate,
               PRODUCT_NAME = p_ProductName,
               PRICE = p_Price,
               DESCRIPTION = p_Description
         WHERE PRODUCT_ID = p_ProductId;

        p_message := 'UPDATE SUCCESSFULLY';
        RETURN;
    ELSE
        INSERT INTO PRODUCT (CREATE_BY,
                                CREATE_DATE,
                                PRODUCT_NAME,
                                PRICE,
                                DESCRIPTION)
             VALUES (p_CreatedBy,
                     p_CreateDate,
                     p_ProductName,
                     p_Price,
                     p_Description);

        p_message := 'INSERT SUCCESSFULLY';
    END IF;
END pro_product_save;
/
```

## Delete Test Product Procedure

Run 
```
CREATE OR REPLACE PROCEDURE ERP.pro_product_delete (
    p_ProductId     IN     VARCHAR2,
    p_message          OUT VARCHAR2)
AS
BEGIN
    IF p_ProductId > 0
    THEN
        DELETE FROM PRODUCT
         WHERE PRODUCT_ID = p_ProductId;

        p_message := 'DELETE SUCCESSFULLY';
        RETURN;
    ELSE
        p_message := 'INVALID PRODUCT ID';
    END IF;
END pro_product_delete;
/
```