﻿using Erp.Domain.Entities.ErpApp.HumanResource.Settings;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
 

namespace Erp.WebApi.Validators
{
    //use of Fluent Validation
    public class UnitValidator : AbstractValidator<Unit>
    {

        public UnitValidator()
        {
            RuleFor(p => p.UnitName)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("Please Provide Unit Name!!!")
                .Length(2, 100).WithMessage("Length of Attribute Name Between 2 to 50 Characters !!!")
                .Must(ValidName).WithMessage("Attribute Name Contains Invalid Characters");


            RuleFor(request => request.UnitId).GreaterThan(0); //this rule create for delete purpose

            RuleFor(request => request.UnitId).NotNull(); // this rule create for update purpose

            //RuleFor(p => p.CreateDate)
            //    .Must(ValidDate).WithMessage("Invalid Date!!!");
        }

        public bool ValidName(string name)
        {
            name = name.Replace(" ", "");
            name = name.Replace("_", "");
            return name.All(char.IsLetter);
        }


        protected bool ValidDate(DateTime date)
        {

            int cNowYear = DateTime.Now.Year;
            int cUpdateYear = date.Year;



            if (cUpdateYear == cNowYear)
            {

                return true;

            }

            return false;

        }

    }
}
