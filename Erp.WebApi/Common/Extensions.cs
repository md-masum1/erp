﻿using Erp.Application.Common.Paging;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Erp.WebApi.Common
{
    public static class Extensions
    {
        public static void AddPagination(this HttpResponse responce, int currentPage, int itemPerPage, int totalItems, int totalPages)
        {
            var paginationHeader = new PaginationHeader(currentPage, itemPerPage, totalItems, totalPages);
            var camelCaseFormatter = new JsonSerializerSettings();
            camelCaseFormatter.ContractResolver = new CamelCasePropertyNamesContractResolver();
            responce.Headers.Add("Pagination", JsonConvert.SerializeObject(paginationHeader, camelCaseFormatter));
            responce.Headers.Add("Access-Control-Expose-Headers", "Pagination");
        }
    }
}
