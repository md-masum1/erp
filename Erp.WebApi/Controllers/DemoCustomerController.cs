﻿using System.Threading.Tasks;
using Erp.Application.Requests.Demo.Commands;
using Erp.Application.Requests.Demo.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Erp.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DemoCustomerController : ControllerBase
    {
        private readonly IMediator _mediator;

        public DemoCustomerController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetDemoCustomer()
        {
            return Ok(await _mediator.Send(new GetDemoCustomerQuery()));
        }

        [HttpPost]
        public async Task<IActionResult> PostDemoCustomer(CreateDemoCustomerCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);

            return BadRequest();
        }
    }
}