﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Erp.Application.Requests.ErpApp.HumanResource.Settings.Department.Queries;
using Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit.Commands;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Erp.WebApi.Controllers.HumanResource
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly IMediator _mediator;

        public DepartmentController(IMediator mediator)
        {

            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));

        }

        [HttpGet]
        public async Task<IActionResult> GetDepartment()
        {
            return Ok(await _mediator.Send(new GetDepartment()));
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> GetDepartmentById(int id)
        {
            return Ok(await _mediator.Send(new GetDepartmentById(id)));
        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateDepartment(CreateUnit command)
        {

            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);
            return BadRequest(result.Errors);



        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDepartment(int id)
        {
            var result = await _mediator.Send(new DeleteUnit (id));

            if (result.Succeeded)
                return NoContent();

            return BadRequest(result.Errors);
        }


    }
}