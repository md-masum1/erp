﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit.Commands;
using Erp.Application.Requests.ErpApp.HumanResource.Settings.Unit.Queries;
using Erp.WebApi.Validators;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Erp.WebApi.Controllers.HumanResource
{
    [Route("api/HumanResource[controller]")]
    [ApiController]
    public class UnitController : ControllerBase
    {

        private readonly IMediator _mediator;

        /// <These object created for Fluent Validation purpose> 
        private readonly Domain.Entities.ErpApp.HumanResource.Settings.Unit _unit = new Domain.Entities.ErpApp.HumanResource.Settings.Unit();
        private readonly UnitValidator _validator = new UnitValidator();
        BindingList<string> _errors = new BindingList<string>();
        /// </end>


        public UnitController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        
        [HttpGet]
        public async Task<IActionResult> GetUnit()
        {
            return Ok(await _mediator.Send(new GetUnit()));
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> GetUnitById(int id)
        {
            return Ok(await _mediator.Send(new GetUnitById (id)));
        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateUnit(CreateUnit command)
        {

            var result = await _mediator.Send(command);

            if (result.Succeeded)
                return StatusCode(201);
            return BadRequest(result.Errors);



        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUnit(int id)
        {
            var result = await _mediator.Send(new DeleteUnit (id));

            if (result.Succeeded)
                return NoContent();

            return BadRequest(result.Errors);
        }



    }
}