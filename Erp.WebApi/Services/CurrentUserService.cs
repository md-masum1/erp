﻿using System;
using System.Security.Claims;
using Erp.Application.Common.Interfaces;
using Microsoft.AspNetCore.Http;

namespace Erp.WebApi.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            UserId = Convert.ToInt32(httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier));
            EmployeeId = httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.SerialNumber);
            UserName = httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.Name);
        }

        public int UserId { get; }
        public string EmployeeId { get; }
        public string UserName { get; }
        public int HeadOfficeId { get; }
        public int BranchOfficeId { get; }

    }
}
